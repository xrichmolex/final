#Use this script to populate the |restaurant| table of the database

import mysql.connector
import json
from decimal import *
from passlib.apps import custom_app_context as pwd_context
import random
import datetime as dt

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'richmole'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME, use_unicode=True)
cursor = cnx.cursor()

#Load json file
inputFile = open('storeData.json','r')
storeDict = json.load(inputFile)
inputFile.close()

print "POPULATING DATABASE %s" %DATABASE_NAME


#Loop through the store items and add info to database
for name, details in storeDict.iteritems():

	inputDict = {
		'name' : name,
		'descr' : details['descr'],
		'image' : details['image'],
		'price' : details['price'],
		'quantity' : details['quantity']
	}

	#Insert this info into the database
	addProduct = ("INSERT INTO products (name, descr, image, price, quantity) VALUES (%(name)s,  %(descr)s, %(image)s, %(price)s, %(quantity)s)")
	cursor.execute(addProduct,inputDict)


# Add Account
hash = pwd_context.encrypt("xw8e2yk3")
addUser = "INSERT into accounts (username, password, email, phone, firstname, lastname) VALUES ('richmole', '%s', 'rmolina1@nd.edu', '9564532186', 'Richard', 'Molina')" %hash
cursor.execute(addUser)

addAddr = "INSERT into addresses (account_id, street, city, state, zip) VALUES (1, '123 Main St.', 'South Bend', 'IN', '46556')"
cursor.execute(addAddr)

# Add order w/ some default items
addOrder = "INSERT into orders (account_id, address_id, price, status) VALUES (1, 1, 99.97, 0)"
cursor.execute(addOrder)

addItem = "INSERT into shipments (product_id, order_id, name, quantity) VALUES (9, 1,'RichMole - The Game', 1)"
cursor.execute(addItem)

addItem = "INSERT into shipments (product_id, order_id, name, quantity) VALUES (8, 1,'Mole Plush - Regular', 1)"
cursor.execute(addItem)

addItem = "INSERT into shipments (product_id, order_id, name, quantity) VALUES (4, 1,'Mole Nerd Shirt', 1)"
cursor.execute(addItem)


cnx.commit()
cnx.close()

print "%s HAS BEEN POPULATED" %DATABASE_NAME

