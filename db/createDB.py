#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'richmole'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, use_unicode=True)
cursor = cnx.cursor()

##########################
## Drop DB if it exists ##
##########################

dropDB = (("DROP DATABASE IF EXISTS %s") % (DATABASE_NAME))
cursor.execute(dropDB)

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET utf8") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to richmole DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################

#shipments
dropTableQuery = ("DROP TABLE IF EXISTS shipments")
cursor.execute(dropTableQuery)

#products
dropTableQuery = ("DROP TABLE IF EXISTS products")
cursor.execute(dropTableQuery)

#orders
dropTableQuery = ("DROP TABLE IF EXISTS orders")
cursor.execute(dropTableQuery)

#addresses
dropTableQuery = ("DROP TABLE IF EXISTS addresses")
cursor.execute(dropTableQuery)

#accounts
dropTableQuery = ("DROP TABLE IF EXISTS accounts")
cursor.execute(dropTableQuery)

########################
## Create tables next ##
########################

#accounts
createTableQuery = ("CREATE TABLE accounts ("
"id INT NOT NULL AUTO_INCREMENT,"
"username VARCHAR(32) CHARACTER SET utf8,"
"password VARCHAR(125) CHARACTER SET utf8 NOT NULL,"
"email VARCHAR(64) CHARACTER SET utf8 NOT NULL,"
"phone VARCHAR(16) CHARACTER SET utf8,"
"firstname VARCHAR(32) CHARACTER SET utf8 NOT NULL,"
"lastname VARCHAR(32) CHARACTER SET utf8 NOT NULL,"
"UNIQUE KEY (email) using BTREE,"
"PRIMARY KEY (id));")
cursor.execute(createTableQuery)

#addresses
createTableQuery = ("CREATE TABLE addresses ("
"id INT NOT NULL AUTO_INCREMENT,"
"account_id INT NOT NULL,"
"street VARCHAR(32) CHARACTER SET utf8 NOT NULL,"
"city VARCHAR(16) CHARACTER SET utf8 NOT NULL,"
"state ENUM ('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY') NOT NULL,"
"zip VARCHAR(8) CHARACTER SET utf8 NOT NULL,"
"PRIMARY KEY (id),"
"FOREIGN KEY (account_id) REFERENCES accounts(id)"
"ON DELETE CASCADE);")
cursor.execute(createTableQuery)

#orders
createTableQuery = ("CREATE TABLE orders ("
"id INT NOT NULL AUTO_INCREMENT,"
"account_id INT NOT NULL,"
"address_id INT NOT NULL,"
"price DECIMAL(8,2) NOT NULL,"
"status INT NOT NULL,"
"PRIMARY KEY (id),"
"FOREIGN KEY (account_id) REFERENCES accounts(id),"
"FOREIGN KEY (address_id) REFERENCES addresses(id)"
"ON DELETE CASCADE);")
cursor.execute(createTableQuery)

#products
createTableQuery = ("CREATE TABLE products ("
"id INT NOT NULL AUTO_INCREMENT,"
"name VARCHAR(32) CHARACTER SET utf8 NOT NULL,"
"descr VARCHAR(128) CHARACTER SET utf8 NOT NULL,"
"image VARCHAR(32) CHARACTER SET utf8 NOT NULL,"
"price DECIMAL(8,2) NOT NULL,"
"quantity INT NOT NULL,"
"PRIMARY KEY (id));")
cursor.execute(createTableQuery)

#shipments
createTableQuery = ("CREATE TABLE shipments ("
"id INT NOT NULL AUTO_INCREMENT,"
"product_id INT NOT NULL,"
"order_id INT NOT NULL,"
"name VARCHAR(32) CHARACTER SET utf8 NOT NULL,"
"quantity INT NOT NULL,"
"PRIMARY KEY (id),"
"FOREIGN KEY (product_id) REFERENCES products(id),"
"FOREIGN KEY (order_id) REFERENCES orders(id)"
"ON DELETE CASCADE);")
cursor.execute(createTableQuery)

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()
