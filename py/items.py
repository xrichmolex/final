''' Implements handler for /items
Imported from handler for /categories/{id} '''
import logging
import cherrypy
import mysql.connector
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import os
import os.path
import json
import pprint
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
from oitemsid import OitemsID

class Items(object):
    ''' Handles resources /restaurants{restID}/categories/{catID}/items}
        Allowed methods: GET, POST, OPTIONS  '''
    exposed = True

    def __init__(self):
        self.id = OitemsID()
        self.db=dict()
        self.db['name']='feednd'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self, restID, menuID, sectID):
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor()
        qn = "select rest_name from restaurants where rest_id=%s" % restID
        cursor.execute(qn)
        restName = cursor.fetchone()[0]
        qn = "select menu_name from menus where menu_id=%s" % menuID
        cursor.execute(qn)
        menuName = cursor.fetchone()[0]
        qn="select section_name from menu_sections where section_id=%s" % sectID
        cursor.execute(qn)
        sectName=cursor.fetchone()[0]
        q="select item_name, item_desc, item_price from menu_items where section_id=%s" % sectID
        cursor.execute(q)
        result=cursor.fetchall()
        return restName, menuName, sectName, result

    def GET(self, restID, menuID, sectID):
        ''' Return list of items for categories for restaurant id'''
        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            restName, menuName, sectName,result=self.getDataFromDB(restID, menuID, sectID)
            pp=pprint.PrettyPrinter(indent=4)
            pp.pprint(result)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            return env.get_template('items-tmpl.html').render(rID=restID,rName=restName,mID=menuID,mName=menuName,\
                                                              sID=sectID,sName=sectName, items=result,\
                                                              base=cherrypy.request.base.rstrip('/') + '/')
        else:
            data = [{
                    'href': '/restaurants/%s/menus/%s/sections/%s/items/%s' % (restID, menuID, sectID, itemName),
                    'name': itemName,
                    'description': desc,
                    'price' : unicode(price)
                    } for itemName, desc, price in result]
            return json.dumps(data, encoding='utf-8')

    def POST(self, restID, menuID, sectID,  **kwargs):
        result= "POST /restaurants/{restID=%s}/categories/{catID=%s}/items     ...     Items.POST\n" % (restID,sectID)
        result+= "POST /restaurants/{restID=%s}/categories/{catID=%s}/items body:\n" % (restID, sectID)
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self,restID, menuID, sectID):
        return "<p>/restaurants/{restID=%s}/categories/{catID=%s}/items allows GET, POST, and OPTIONS</p>"
