''' Implements handler for /orders/{orderID}/
To add an item to an order '''
import apiutil
import cherrypy
import mysql.connector
from mysql.connector import Error
import sys
import os
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import json
from apiutil import errorJSON
from config import conf
from oitemsid import OitemsID

class Oitems(object):
    exposed = True

    def __init__(self):
        self.id = OitemsID()
        self.db = dict()
        self.db['name'] = 'feednd'
        self.db['user'] = 'root'
        self.db['host'] = '127.0.0.1'
    # 
    # def _cp_dispatch(self,vpath):
    #     print "Orders._cp_dispatch with vpath: %s \n" % vpath
    #     if len(vpath) == 3: # /orders/{orderID}/items/{itemID}
    #         cherrypy.request.params['orderID']=vpath.pop(0)
    #         vpath.pop(0) # items
    #         itemID = vpath.pop(0).split('-')
    #         cherrypy.request.params['sectionID'] = itemID[0]
    #         cherrypy.request.params['itemName'] = itemID[1]
    #         return self
    #     return vpath

    def getDataFromDB(self, orderID, userID=1):
        cnx = mysql.connector.connect(user=self.db['user'], host=self.db['host'], database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor()

        qn = "select user_firstname from users where user_id=%s" % userID
        cursor.execute(qn)
        userName = cursor.fetchone()[0]
        qn = "select order_date, order_time from orders where order_id=%s" % orderID
        cursor.execute(qn)
        orderStuff = cursor.fetchone()
        orderName = str(orderStuff[0]) + '-' + str(orderStuff[1])
        q = "select section_id, item_name, item_quantity from order_items where order_id=%s" % \
            orderID
        cursor.execute(q)
        result = cursor.fetchall()
        return userName, orderName, result

    def GET(self, orderID, userID=1):
        ''' Return list of orders'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            userName, orderName, result = self.getDataFromDB(orderID, userID)
        except mysql.connector.Error as e:
            raise e

        if output_format == 'text/html':
            return env.get_template('oitems-tmpl.html').render(
                uID=userID,
                uName=userName,
                oID=orderID,
                oName=orderName,
                orderItems=result,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            data = [{
                        'href': 'orders/%s/items/%s-%s' % (orderID, section_id, item_name.replace(' ', '_')),
                        'name': userName,
                        'order': orderID,
                        'quantity': item_quantity
                    } for section_id, item_name, item_quantity in result]
            return json.dumps(data, encoding='utf-8')

    def POST(self, **kwargs):
        result = "POST /orders   ...     Orders.POST\n"
        result += "POST /orders body:\n"
        for key, value in kwargs.items():
            result += "%s = %s \n" % (key, value)
        # Validate form data
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self):
        return "<p>/orders allows GET, POST, and OPTIONS</p>"
