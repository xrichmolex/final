''' Implements handler for /store'''
import logging
import cherrypy
import mysql.connector
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import os
import os.path
import json
import pprint
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/../html/'))
from config import conf

class Store(object):
    ''' Handles resources /store
        Allowed methods: GET, POST, OPTIONS  '''
    exposed = True

    def __init__(self):
        self.db=dict()
        self.db['name']='richmole'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self):
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor();
        q="select id, name, descr, price, image from products order by price desc"
        cursor.execute(q)
        result=cursor.fetchall()
        q="select id, name, quantity from shipments where order_id=1"
        cursor.execute(q)
        ships=cursor.fetchall()
        q="select price from orders where id=1"
        cursor.execute(q)
        total=cursor.fetchone()
        cnx.commit()
        cnx.close()
        return result, ships, unicode(total[0])

    def GET(self):
        ''' Return list of items for store'''
        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            result,ships,total=self.getDataFromDB()
            pp=pprint.PrettyPrinter(indent=4)
            pp.pprint(result)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            return env.get_template('store-tmpl.html').render(items=result, ships=ships, total=total, base=cherrypy.request.base.rstrip('/') + '/')
        else:
            data = [{
                    'id': idd,
                    'name': name,
                    'description': description,
                    'price' : unicode(price),
                    'image' : image
                    } for idd, name, description, price, image in result]
            return json.dumps(data, encoding='utf-8')

    def POST(self, item, **kwargs):
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor();
        q="select name, price from products where id=%s" % item
        cursor.execute(q)
        ship=cursor.fetchone()
        q = "insert into shipments (order_id, product_id, name, quantity) values ('1', '%s', '%s', '1')" % (item, ship[0])
        cursor.execute(q)
        q = "select price from orders where id=1"
        cursor.execute(q)
        oldP = cursor.fetchone()[0]
        newP = oldP + ship[1]
        q = "update orders set price=%s where id=1" % newP
        cursor.execute(q)
        cnx.commit()
        cnx.close()
        return self.GET()

    def DELETE(self, item, **kwargs):
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor();
        q="select product_id, quantity from shipments where id=%s" % item
        cursor.execute(q)
        ship = cursor.fetchone()
        q = "select price from products where id=%s" % ship["product_id"]
        cursor.execute(q)
        price = cursor.fetchone()
        q="delete from shipments where id=%s" % item
        cursor.execute(q)
        q = "select price from orders where id=1"
        cursor.execute(q)
        oldP = cursor.fetchone()
        newP = oldP - (price * ship["quantity"])
        q = "update orders set price=%s where id=1" % newP
        cursor.execute(q)
        cnx.commit()
        cnx.close()
        return item

    def OPTIONS(self):
        return "<p>/store allows GET, POST, DELETE, and OPTIONS</p>"

class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    }
    cherrypy.tree.mount(Store(), '/store', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(Store(), None, conf)

