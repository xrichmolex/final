''' /demo resource for richmole.net
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Casey Ferris and Richard Molina
Date: April 2015
Web Applications'''

import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import mysql.connector
import os
import os.path
import math
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/../html/'))
import logging
from config import conf

class Demo(object):
    ''' Handles resource /demo
        Allowed methods: GET'''
    exposed = True

    def __init__(self):
        self.db = dict()
        self.db['name']='richmole'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self,vpath):
            print "Demo._cp_dispatch with vpath: %s \n" % vpath
            if len(vpath) == 0: # /demo
                return self
            return vpath

    def getDataFromDB(self):
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor();
        q="select id, name, descr, price, image from products"
        cursor.execute(q)
        result=cursor.fetchall()
        q="select id, name, quantity from shipments where order_id=1"
        cursor.execute(q)
        ships=cursor.fetchall()
        q="select price from orders where id=1"
        cursor.execute(q)
        total=cursor.fetchone()
        cnx.commit()
        cnx.close()
        return result, ships, unicode(total[0])

    def GET(self):

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML
        output_format = cherrypy.lib.cptools.accept(['text/html'])

        if output_format == 'text/html':
            result, ships, total = self.getDataFromDB()
            return env.get_template('demo-tmpl.html').render(items=result, ships=ships, total=total, base=cherrypy.request.base.rstrip('/') + '/')

    def DELETE(self, item, **kwargs):
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor();
        q="select product_id, quantity from shipments where id=%s" % item
        cursor.execute(q)
        ship = cursor.fetchone()
        q = "select price from products where id=%s" % ship["product_id"]
        cursor.execute(q)
        price = cursor.fetchone()
        q="delete from shipments where id=%s" % item
        cursor.execute(q)
        q = "select price from orders where id=1"
        cursor.execute(q)
        oldP = cursor.fetchone()
        newP = oldP - (price * ship["quantity"])
        q = "update orders set price=%s where id=1" % newP
        cursor.execute(q)
        cnx.commit()
        cnx.close()
        return item

    def OPTIONS(self):
        ''' Allows GET and OPTIONS '''
        #Prepare response
        return "<p>/demo/ allows GET and OPTIONS</p>"


class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    }
    cherrypy.tree.mount(Demo(), '/demo', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(Demo(), None, conf)

