''' Implements handler for /categories/{id}
Imported from handler for /categories'''
import cherrypy
from items import Items
class SectionID(object):
    ''' Handles resource /categories/{restID}/{catID}
        Allowed methods: GET, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.items = Items()

    def GET(self, restID, menuID, sectID):
        ''' Return info of section id for restaurant id'''
        return "GET /restaurants/{restID=%s}/menus/{menuID=%s}/sections/{sectID=%s}  ...   CategoryID.GET" % (restID,
                                                                                                            menuID,
                                                                                                            sectID)

    def PUT(self, restID, menuID, sectID, **kwargs):
        ''' Update category id for restaurant id'''
        result = "PUT /restaurants/{restID=%s}/menus/{menuID=%s}/sections/{sectID=%s}   ...   CategoryID.PUT\n" % (
            restID, menuID, sectID)
        result += "PUT /restaurants/{restID=%s}/menus/{menuID=%s}/sections/{sectID=%s} body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update restaurant
        # Prepare response
        return result

    def DELETE(self, restID, menuID, sectID):
        #Validate id
        #Delete restaurant
        #Prepare response
        return "DELETE /restaurants/{restID=%s}/menus/{menuID=%s}/sections/{sectID=%s}   ...   CategoryID.DELETE" % (
            restID, menuID, sectID)

    def OPTIONS(self, restID, menuID, sectID):
        return "<p>/restaurants/{restID}/menus/{menuID}/sections/{sectID} allows GET, PUT, DELETE, and OPTIONS</p>"

