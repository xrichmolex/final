import cherrypy
import mysql.connector
from mysql.connector import Error
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import json
from apiutil import errorJSON
from passlib.apps import custom_app_context as pwd_context
from config import conf
import os
import os.path
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/../html/'))

class UserLogin(object):
    exposed = True

    def GET(self):
        ''' return user login form '''

        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        if output_format == 'text/html':
            return env.get_template('login-tmpl.html').render(
                base=cherrypy.request.base.rstrip('/') + '/'
                )


    @cherrypy.tools.json_in(force=False)
    def POST(self,username=None,password=None):
        ''' login existing user
        should only be used through SSL connection
        expects JSON:
        { 'username' : username,
          'password' : password
        }
        return JSON:
        { 'errors' : [] }
        error code / message:
        5000 : Expected username and password
        5001 : Incorrect username or password
        '''
        if not username:
            try:
                username = cherrypy.request.json["username"]
            except:
                return errorJSON(code=5000, message="Expected username and password")
        if not password:
            try:
                password = cherrypy.request.json["password"]
            except:
                return errorJSON(code=5000, message="Expected username and password")
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='richmole',charset='utf8mb4')   
        cursor = cnx.cursor()
        q="select password from accounts where username=%s";
        cursor.execute(q,(username,))
        r=cursor.fetchall()
        if len(r) == 0:
            return errorJSON(code=5001, message="Incorrect username or password")
        hash=r[0][0]
        match=pwd_context.verify(password,hash)
        print "Hash: %s \nmatch %s" % (hash,match)
        password=""
        if not match:
            # username / hash do not exist in database
            return errorJSON(code=5001, message="Incorrect username or password")
        else:
            # username / password correct
            result={'errors':[]}
            return json.dumps(result)

application = cherrypy.Application(UserLogin(), None, conf)
