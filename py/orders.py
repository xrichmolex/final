''' Implements handler for /orders/{orderID}/
To add an item to an order '''
import apiutil
import cherrypy
from jinja2 import Environment, FileSystemLoader
import mysql.connector
from mysql.connector import Error
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import os, os.path
import json
from apiutil import errorJSON
from orderid import OrderID
from config import conf

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__)) + '/templates/'))

class Orders(object):
    exposed = True

    def __init__(self):
        self.id = OrderID()
        self.db = dict()
        self.db['name'] = 'feednd'
        self.db['user'] = 'root'
        self.db['host'] = '127.0.0.1'

    def _cp_dispatch(self,vpath):
        print "Orders._cp_dispatch with vpath: %s \n" % vpath
        if len(vpath) == 1:  # /orders/{orderID}
            cherrypy.request.params['orderID'] = vpath.pop(0)
            return self.id
        if len(vpath) == 2:  # /orders/{orderID}/items
            cherrypy.request.params['orderID'] = vpath.pop(0)
            vpath.pop(0)  # items
            return self.id.oitems
        if len(vpath) == 3:  # /orders/{orderID}/items/{itemID}
            cherrypy.request.params['orderID'] = vpath.pop(0)
            vpath.pop(0)  # items
            orItemID = vpath.pop(0).split('-')
            cherrypy.request.params['sectionID'] = orItemID[0]
            cherrypy.request.params['itemName'] = orItemID[1]
            return self.id.oitems.id
        return vpath

    def getDataFromDB(self, userID=1):
        cnx = mysql.connector.connect(user=self.db['user'], host=self.db['host'], database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor()

        qn = "select user_firstname from users where user_id=%s" % userID
        cursor.execute(qn)
        userName = cursor.fetchone()[0]
        q = "select order_id, order_price, order_date, order_time from orders where user_id=%s" % userID
        cursor.execute(q)
        result = cursor.fetchall()
        return userName, result

    def GET(self, userID=1):
        ''' Return list of orders'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            userName, result = self.getDataFromDB(userID)
        except mysql.connector.Error as e:
            raise e

        if output_format == 'text/html':
            return env.get_template('orders-tmpl.html').render(
                uID=userID,
                uName=userName,
                orders=result,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            data = [{
                        'href': 'orders/%s/items' % (order_id),
                        'name': userName,
                        'price': order_price,
                        'date': order_date,
                        'time': order_time
                    } for order_id, order_price, order_date, order_time in result]
            return json.dumps(data, encoding='utf-8')

    def POST(self, **kwargs):
        result = "POST /orders   ...     Orders.POST\n"
        result += "POST /orders body:\n"
        for key, value in kwargs.items():
            result += "%s = %s \n" % (key, value)
        # Validate form data
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self):
        return "<p>/orders allows GET, POST, and OPTIONS</p>"

application = cherrypy.Application(Orders(), None, conf)
