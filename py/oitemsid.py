''' Implements handler for /orders/{orderID}/
To add an item to an order '''
import apiutil
import cherrypy
import mysql.connector
from mysql.connector import Error
import sys

sys.stdout = sys.stderr  # Turn off console output; it will get logged by Apache
import json
from apiutil import errorJSON
from config import conf


class OitemsID(object):
    exposed = True

    def __init__(self):
        self.db = dict()
        self.db['name'] = 'feednd'
        self.db['user'] = 'root'
        self.db['host'] = '127.0.0.1'

    #
    # def _cp_dispatch(self,vpath):
    # print "Orders._cp_dispatch with vpath: %s \n" % vpath
    #     if len(vpath) == 3: # /orders/{orderID}/items/{itemID}
    #         cherrypy.request.params['orderID']=vpath.pop(0)
    #         vpath.pop(0) # items
    #         itemID = vpath.pop(0).split('-')
    #         cherrypy.request.params['sectionID'] = itemID[0]
    #         cherrypy.request.params['itemName'] = itemID[1]
    #         return self
    #     return vpath

    def GET(self, orderID, sectionID, itemName):
        ''' GET orderItemID,quantity for orderID and itemID or error if no entry with orderID or itemID'''
        cnx = mysql.connector.connect(user=self.db['user'], host=self.db['host'], database=self.db['name'],
                                      charset='utf8mb4')
        cursor = cnx.cursor()
        q = "select exists(select 1 from order_items where order_id=%s and section_id=%s and item_name=%s)" % (orderID,
                                                                                                               sectionID,
                                                                                                               itemName)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #no orderItemID for this orderID / itemID combination
            return errorJSON(code=9004, message="OrderItemID for OrderID %s  and ItemID %s Does Not Exist" % (
                orderID, itemName))
        q = "select item_price, item_quantity from order_items where order_id=%s and section_id=%s and item_name=%s" % (
            orderID,
            sectionID,
            itemName)
        cursor.execute(q)
        tmp = cursor.fetchall()
        result = {"item_price": tmp[0][0], "item_quantity": tmp[0][1], "errors": []}
        return json.dumps(result)

    def DELETE(self, orderID, sectionID, itemName):
        ''' Delete orderItem with orderID and itemID'''
        cnx = mysql.connector.connect(user=self.db['user'], host=self.db['host'], database=self.db['database'],
                                      charset='utf8mb4')
        cursor = cnx.cursor()
        q = "set sql_safe_updates=0;"
        cursor.execute(q)
        q = "select exists(select 1 from order_items where orderID=%s and sectionID=%s and itemName=%s)" % (orderID,
                                                                                                            sectionID,
                                                                                                            itemName)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #no orderItemID for this orderID / itemID combination
            return errorJSON(code=9004, message="OrderItemID for OrderID %s  and ItemID %s Does Not Exist" % (
                orderID, itemName))
        try:
            q = "delete from order_items where orderID=%s and sectionID=%s and itemName=%s" % (
                orderID,
                sectionID,
                itemName)
            cursor.execute(q)
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert orderItem
            print "mysql error: %s" % e
            return errorJSON(code=9005, message="Failed to delete order item from shopping cart")
        result = {"errors": []}
        return json.dumps(result)

    @cherrypy.tools.json_in(force=False)
    def PUT(self, orderID, sectionID, itemName):
        ''' Add or update an item to an order 
        quantity is received in a JSON dictionary
        output is also returned in a JSON dictionary'''
        try:
            quantity = int(cherrypy.request.json["quantity"])
            print "quantity received: %s" % quantity
        except:
            print "quantity was not received"
            return errorJSON(code=9003, message="Expected integer 'quantity' of items in order as JSON input")
        cnx = mysql.connector.connect(user=self.db['user'], host=self.db['host'], database=self.db['database'],
                                      charset='utf8mb4')
        cursor = cnx.cursor()
        q = "set sql_safe_updates=0;"
        cursor.execute(q)
        # does orderID exist?
        q = "select exists(select 1 from orders where order_id=%s)" % orderID
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #orderID does not exist
            return errorJSON(code=9000, message="Order with OrderID %s Does Not Exist") % orderID
        # does itemID exist?
        q = "select exists(select 1 from items where section_id=%s and item_name=%s)" % (sectionID, itemName)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #itemID does not exist
            return errorJSON(code=9001, message="Item with ItemID %s Does Not Exist") % itemName
        q = "insert into order_items (order_id, section_id, item_name, quantity) values (%s, %s, %s, %s) on duplicate " \
            "key update " \
            "quantity=%s;" \
            % (orderID, sectionID, itemName, quantity, quantity)
        cursor.execute(q)
        q = "select item_price from order_items where order_id=%s and section_id=%s and item_name=%s" % (orderID,
                                                                                                         sectionID,
                                                                                                         itemName)
        itemPrice = 0.0
        try:
            cursor.execute(q)
            itemPrice = cursor.fetchall()[0][0]
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert orderItem
            print "mysql error: %s" % e
            return errorJSON(code=9002, message="Failed to add order item to shopping cart")
        result = {'orderID': orderID, 'sectionID': sectionID, 'itemID': itemName, 'quantity': quantity,
                  'itemPrice': itemPrice, 'errors': []}
        return json.dumps(result)
