''' Implements handler for /sections
Imported from handler for /restaurants/{id}/menus/{id} '''

import os, os.path, json, logging, mysql.connector

import cherrypy
from jinja2 import Environment, FileSystemLoader

from sectionid import SectionID

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Sections(object):
    ''' Handles resources /categories/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.id = SectionID()
        self.db=dict()
        self.db['name']='feednd'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self,restID,menuID):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
            charset='utf8mb4'
        )
        cursor = cnx.cursor()
        qn = "select rest_name from restaurants where rest_id=%s" % restID
        cursor.execute(qn)
        restName = cursor.fetchone()[0]
        qn="select menu_name from menus where menu_id=%s" % menuID
        cursor.execute(qn)
        menuName=cursor.fetchone()[0]
        q="select section_id, section_name from menu_sections where menu_id=%s order by section_id" % menuID
        cursor.execute(q)
        result=cursor.fetchall()
        return restName,menuName,result

    def GET(self, restID, menuID):
        ''' Return list of sections for restaurant menu menuID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            restName,menuName,result=self.getDataFromDB(restID,menuID)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            return env.get_template('sections-tmpl.html').render(
                rID=restID,
                rName = restName,
                mID=menuID,
                mName=menuName,
                sections=result,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            data = [{
                'href': 'restaurants/%s/menus/%s/sections/%s/items' % (restID, menuID, section_id),
                'name': section_name
            } for section_id, section_name in result]
            return json.dumps(data, encoding='utf-8')

    def POST(self, **kwargs):
        result= "POST /restaurants/{restID}/menus/{menuID}/sections     ...     Sections.POST\n"
        result+= "POST /restaurants/{restID}/menus/{menuID}/sections body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self,restID):
        return "<p>/restaurants/{restID}/menus/{menuID}/sections allows GET, POST, and OPTIONS</p>"
