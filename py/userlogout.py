import cherrypy
import apiutil
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import json
from apiutil import errorJSON
from config import conf

SESSION_KEY = '_cp_username'

class UserLogout(object):
    exposed = True

    def POST(self):
        sess = cherrypy.session
        username = sess.get(SESSION_KEY, None)
        print "logout SESSION KEY %s" % SESSION_KEY
        print "logout value of SESSION KEY %s" % cherrypy.session[SESSION_KEY]
        sess[SESSION_KEY] = None
        if username:
            print "found username %s of session %s" % (username, SESSION_KEY)
            cherrypy.request.login = None
        r={'errors':[]}
        return json.dumps(r)

application = cherrypy.Application(UserLogout(), None, conf)
        
