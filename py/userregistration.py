''' /registration resource for richmole.net
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Casey Ferris and Richard Molina
Date: April 2015
Web Applications'''

import apiutil
from apiutil import errorJSON
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import json
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/../html'))
import logging
from passlib.apps import custom_app_context as pwd_context
from config import conf
from pyvalidate import validate, ValidationException

class UserRegistration(object):
    ''' Handles resource /users/registration
        Allowed methods: GET, POST, OPTIONS '''
    exposed = True

    def __init__(self):
        self.db = dict()
        self.db['name']='richmole'
        self.db['user']='root'
        self.db['host']='127.0.0.1'


    def GET(self):
        ''' Prepare user registration page '''

        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        if output_format == 'text/html':
            return env.get_template('registration-tmpl.html').render()

    @validate(requires=['username', 'password', 'firstname', 'lastname', 'address', 'city', 'state', 'zipcode', 'email', 'phone'],
              types={'username':str, 'password':str, 'firstname':str, 'lastname':str, 'address':str, 'city':str, 'state':str, 'zipcode':str, 'email':str, 'phone':str},
              values={'email':'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', 'phone':'^\d*$', 'zipcode':'^\d{5}(?:[-\s]\d{4})?$'}
             )
    def check_params(self, username, password, firstname, lastname, address, city, state, zipcode, email, phone):
              print 'adding user "%s:%s" with username: %s:%s email: %s:%s phone: %s:%s password: %s:%s' % (firstname, type(firstname),
			 username, type(username),
                         email, type(email),
                         phone, type(phone),
                         password, type(password))

    @cherrypy.tools.json_in(force=False)
    def POST(self,username=None,password=None,confirm=None,firstname=None,lastname=None,address=None,city=None,state=None,zipcode=None,email=None,phone=None):
        ''' Add a new user '''

	print "rmolina1:in POST"
	# Check for username
        if not username:
          try:
            username = str(cherrypy.request.json["username"])
            print "username received: %s" % firstname
          except:
            print "username was not received"
            return errorJSON(code=9003, message="Expected text 'username' for user as JSON input")

	# Check for firstname
        if not firstname:
          try:
            firstname = str(cherrypy.request.json["firstname"])
            print "firstname received: %s" % firstname
          except:
            print "firstname was not received"
            return errorJSON(code=9003, message="Expected text 'firstname' for user as JSON input")

	# Check for lastname
        if not lastname:
          try:
            lastname = str(cherrypy.request.json["lastname"])
            print "lastname received: %s" % lastname
          except:
            print "lastname was not received"
            return errorJSON(code=9003, message="Expected text 'lastname' for user as JSON input")

	# Check for address
        if not address:
          try:
            address = str(cherrypy.request.json["address"])
            print "address received: %s" % address
          except:
            print "address was not received"
            return errorJSON(code=9003, message="Expected text 'address' for user as JSON input")

	# Check for city
        if not city:
          try:
            city = str(cherrypy.request.json["city"])
            print "city received: %s" % city
          except:
            print "city was not received"
            return errorJSON(code=9003, message="Expected text 'city' for user as JSON input")

	# Check for state
        if not state:
          try:
            state = str(cherrypy.request.json["state"])
            print "state received: %s" % state
          except:
            print "state was not received"
            return errorJSON(code=9003, message="Expected text 'state' for user as JSON input")

	# Check for zipcode
        if not zipcode:
          try:
            zipcode = str(cherrypy.request.json["zipcode"])
            print "zipcode received: %s" % zipcode
          except:
            print "zipcode was not received"
            return errorJSON(code=9003, message="Expected text 'zipcode' for user as JSON input")

	# Check for email
        if not email:
          try:
            email = cherrypy.request.json["email"]
            print "email received: %s" % email
          except:
            print "email was not received"
            return errorJSON(code=9003, message="Expected email 'email' for user as JSON input")

	# Check for phone
        if not phone:
          try:
            phone = cherrypy.request.json["phone"]
            print "phone received: %s" % phone
          except:
            print "phone was not received"
            return errorJSON(code=9003, message="Expected tel 'phone' for user as JSON input")

	# Check for password
        if not password:
          try:
            password = cherrypy.request.json["password"]
            #print "password received: %s" % password
          except:
            print "password was not received"
            return errorJSON(code=9003, message="Expected password 'password' for user as JSON input")

	# Check for confirmation
        if not confirm:
          try:
            confirm = cherrypy.request.json["confirm"]
            #print "confirm received: %s" % confirm
          except:
            print "password confirmation was not received"
            return errorJSON(code=9003, message="Expected Password confirmation 'confirm' for user as JSON imput")


        try:
            self.check_params(username=username,password=password,firstname=firstname,lastname=lastname,address=address,city=city,state=state,zipcode=zipcode,email=email,phone=phone)

        except ValidationException as ex:
            print ex.message
            return errorJSON(code=9003, message=ex.message)

	# Establish db Connection
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'])
        cursor = cnx.cursor()

	# Password confirmation
        if confirm != password:
            return errorJSON(code=9004, message="Passwords do not Match")
	confirm = ''

	# Check if username already exists
        q="SELECT EXISTS(SELECT 1 FROM accounts WHERE username='%s')" % username
        cursor.execute(q)
        if cursor.fetchall()[0][0]:
            #username already exists
            print "User with username %s Already Exists" % username
            return errorJSON(code=9000, message="User with username %s Already Exists") % username

        # Check if email already exists
        q="SELECT EXISTS(SELECT 1 FROM accounts WHERE email='%s')" % email
        cursor.execute(q)
        if cursor.fetchall()[0][0]:
            #email already exists
            print "User with email %s Already Exists" % email
            return errorJSON(code=9000, message="User with email %s Already Exists") % email

        hash = pwd_context.encrypt(password)
        password = ''
	print "rmolina: inserting into db"

        q="INSERT INTO accounts (username, password, email, phone, firstname, lastname) VALUES ('%s', '%s', '%s', '%s', '%s', '%s');" \
            % (username, hash, email, phone, firstname, lastname)
        try:
            cursor.execute(q)
	    print "rmolina: cursor executed"
            qid="SELECT id FROM accounts WHERE email='%s'" % email
	    cursor.execute(qid)
	    print "rmolina: fetor executed"
            userID=cursor.fetchall()[0][0]
	    print "rmolina: userID = %s" %userID
	    q="INSERT INTO addresses (account_id, street, city, state, zip) VALUES ('%s', '%s', '%s', '%s', '%s');" \
            % (userID, address, city, state, zipcode)
	    cursor.execute(q)
	    

            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert user
            print "mysql error: %s" % e
            return errorJSON(code=9002, message="Failed to add user")
        result = {'username':username, 'firstname':firstname, 'lastname':lastname, 'email':email, 'password':hash, 'phone':phone, 'errors':[]}
        return json.dumps(result)

    def OPTIONS(self):
        ''' Allows GET, POST, OPTIONS '''
        #Prepare response
        return "<p>/register allows GET, POST, and OPTIONS</p>"

class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    }
    cherrypy.tree.mount(UserRegistration(), '/users/registration', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(UserRegistration(), None, conf)


